import { DataEvent, EventArgs } from './event';

export class LayersChangedEvent extends DataEvent {
  /**
   *
   * @callback LayersChangedEventHandler
   * @param {LayersChangedEventArgs} eventArgs
   */

  /**
   *
   * @param {LayersChangedEventHandler} handler - A callback to run.
   */
  addHandler(handler) {
    super.addHandler(handler);
  }

  /**
   *
   * @param {LayersChangedEventArgs} eventArgs
   */
  fire(eventArgs) {
    super.fire(eventArgs);
  }
}

export class LayersChangedEventArgs extends EventArgs {
  constructor(layersNames = [], currentLayerName = '') {
    super();
    this.layersNames = layersNames;
    this.currentLayerName = currentLayerName;
  }
}
