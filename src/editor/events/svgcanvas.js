import { DataEvent } from './event';

export class MouseDownEvent extends DataEvent {
  /**
   *
   * @callback MouseDownEventHandler
   * @param {MouseEvent} eventArgs
   */

  /**
   *
   * @param {MouseDownEventHandler} handler - A callback to run.
   */
  addHandler(handler) {
    super.addHandler(handler);
  }

  /**
   *
   * @param {MouseEvent} eventArgs
   */
  fire(eventArgs) {
    super.fire(eventArgs);
  }
}

export class MouseUpEvent extends DataEvent {
  /**
   *
   * @callback MouseUpEventHandler
   * @param {MouseEvent} eventArgs
   */

  /**
   *
   * @param {MouseUpEventHandler} handler - A callback to run.
   */
  addHandler(handler) {
    super.addHandler(handler);
  }

  /**
   *
   * @param {MouseEvent} eventArgs
   */
  fire(eventArgs) {
    super.fire(eventArgs);
  }
}

export class MouseMoveEvent extends DataEvent {
  /**
   *
   * @callback MouseMoveEventHandler
   * @param {MouseEvent} eventArgs
   */

  /**
   *
   * @param {MouseMoveEventHandler} handler - A callback to run.
   */
  addHandler(handler) {
    super.addHandler(handler);
  }

  /**
   *
   * @param {MouseEvent} eventArgs
   */
  fire(eventArgs) {
    super.fire(eventArgs);
  }
}
