import { Svg } from '@svgdotjs/svg.js';
import { DataEvent, EventArgs } from './event';

export class ElementSelectedEvent extends DataEvent {
  /**
   *
   * @callback ElementSelectedEventHandler
   * @param {ElementSelectedEventArgs} eventArgs
   */

  /**
   *
   * @param {ElementSelectedEventHandler} handler - A callback to run.
   */
  addHandler(handler) {
    super.addHandler(handler);
  }

  /**
   *
   * @param {ElementSelectedEventArgs} eventArgs
   */
  fire(eventArgs) {
    super.fire(eventArgs);
  }
}

export class ElementSelectedEventArgs extends EventArgs {
  /**
   *
   * @param {Svg} element
   */
  constructor(element) {
    super();
    this.element = element;
  }
}

export class ElementUnselectedEvent extends DataEvent {
  /**
   *
   * @callback ElementUnselectedEventHandler
   * @param {ElementUnselectedEventArgs} eventArgs
   */

  /**
   *
   * @param {ElementUnselectedEventHandler} handler - A callback to run.
   */
  addHandler(handler) {
    super.addHandler(handler);
  }

  /**
   *
   * @param {ElementUnselectedEventArgs} eventArgs
   */
  fire(eventArgs) {
    super.fire(eventArgs);
  }
}

export class ElementUnselectedEventArgs extends EventArgs {
  /**
   *
   * @param {Svg} element
   */
  constructor(element) {
    super();
    this.element = element;
  }
}

export class SelectionChangedEvent extends DataEvent {
  /**
   *
   * @callback SelectionChangedEventHandler
   * @param {SelectionChangedEventArgs} eventArgs
   */

  /**
   *
   * @param {SelectionChangedEventHandler} handler - A callback to run.
   */
  addHandler(handler) {
    super.addHandler(handler);
  }

  /**
   *
   * @param {SelectionChangedEventArgs} eventArgs
   */
  fire(eventArgs) {
    super.fire(eventArgs);
  }
}

export class SelectionChangedEventArgs extends EventArgs {
  /**
   *
   * @param {Array<Svg>} selectedSvgElements
   */
  constructor(selectedSvgElements) {
    super();
    this.selectedSvgElements = selectedSvgElements;
  }
}
