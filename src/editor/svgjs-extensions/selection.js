import { Element, Svg, extend } from '@svgdotjs/svg.js';
import {
  ElementSelectedEventArgs,
  ElementUnselectedEventArgs,
} from '../events/selection';

class SelectionHandler {
  constructor(el) {
    el.remember('_selection', this);
    this.el = el;
    this.onClick = this.onClick.bind(this);
    this.elementSelectedEvent = null;
    this.elementUnselectedEvent = null;
    this.isElementSelected = false;
    this.isElementSelectable = false;
  }

  onClick(e) {
    if (this.isElementSelectable) {
      if (e.ctrlKey && !this.isElementSelected) {
        this.select();
      } else if (e.ctrlKey && this.isElementSelected) {
        this.unselect();
      }
    }
  }

  unselect() {
    if (this.isElementSelectable) {
      this.isElementSelected = false;

      if (this.elementUnselectedEvent)
        this.elementUnselectedEvent.fire(
          new ElementUnselectedEventArgs(this.el)
        );
    }
  }
  select() {
    if (this.isElementSelectable) {
      this.isElementSelected = true;

      if (this.elementSelectedEvent)
        this.elementSelectedEvent.fire(new ElementSelectedEventArgs(this.el));
    }
  }

  init(selectable, elementSelectedEvent, elementUnselectedEvent) {
    this.isElementSelectable = selectable;
    if (!!elementSelectedEvent)
      this.elementSelectedEvent = elementSelectedEvent;
    if (!!elementUnselectedEvent)
      this.elementUnselectedEvent = elementUnselectedEvent;
    if (selectable) {
      this.el.on('click', this.onClick, { passive: false });
    } else {
      this.el.off('click', this.onClick);
    }
  }
}

extend(Element, {
  /**
   *
   * @returns {Svg}
   */
  selection({
    selectable = true,
    elementSelectedEvent,
    elementUnselectedEvent,
  }) {
    const selection = this.remember('_selection') || new SelectionHandler(this);
    selection.init(selectable, elementSelectedEvent, elementUnselectedEvent);
    return this;
  },
  unselect() {
    const selection = this.remember('_selection');
    if (!!selection) selection.unselect();
    return this;
  },
  select() {
    const selection = this.remember('_selection');
    if (!!selection) selection.select();
    return this;
  },
});
