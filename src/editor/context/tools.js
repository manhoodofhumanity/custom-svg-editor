import { TOOLS } from '../constants/tools';
import { ToolChangedEvent, ToolChangedEventArgs } from '../events/tools';

export class ToolsContext {
  constructor() {
    /**
     * @private
     */
    this.currentTool = null;
    this.toolChangedEvent = new ToolChangedEvent();
  }

  /**
   * @param {string} tool
   */
  selectCurrentTool(tool) {
    this.currentTool = tool;

    this.toolChangedEvent.fire(new ToolChangedEventArgs(this.currentTool));
  }

  /**
   * @returns {boolean}
   */
  isCurrentToolForDrawing() {
    return (
      this.currentTool !== TOOLS.SELECTION &&
      this.currentTool !== TOOLS.BUILD_L_SYSTEM_RESULT &&
      this.currentTool !== TOOLS.ZOOM
    );
  }

  /**
   * @returns {string}
   */
  getCurrentTool() {
    return this.currentTool;
  }
}
