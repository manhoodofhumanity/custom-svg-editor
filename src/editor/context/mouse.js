import { Point } from '@svgdotjs/svg.js';
import { DrawingContext } from './drawing';
import { SvgCanvasContext } from './svgcanvas';
import { ToolsContext } from './tools';

export class MouseContext {
  /**
   *
   * @param {SvgCanvasContext} svgcanvasContext
   * @param {ToolsContext} toolsContext
   * @param {DrawingContext} drawingContext
   */
  constructor(svgcanvasContext, toolsContext, drawingContext) {
    /**
     * @private
     */
    this._svgcanvasContext = svgcanvasContext;
    /**
     * @private
     */
    this._toolsContext = toolsContext;
    /**
     * @private
     */
    this._drawingContext = drawingContext;

    // flag that mouse is now tapped and not released yet
    this.isMouseDown = false;
    this.mouseTapStartPoint = null;
    this.mouseTapEndPoint = null;
  }

  setIsMouseDown(isMouseDown) {
    this.isMouseDown = isMouseDown;
  }

  /**
   *
   * @param {Point} tapStartPoint
   */
  setMouseTapStartPoint(tapStartPoint) {
    this.mouseTapStartPoint = tapStartPoint;
  }
  /**
   *
   * @returns {Point}
   */
  getMouseTapStartPoint() {
    return this.mouseTapStartPoint;
  }
  /**
   *
   * @param {Point} tapEndPoint
   */
  setMouseTapEndPoint(tapEndPoint) {
    this.mouseTapEndPoint = tapEndPoint;
  }
  /**
   *
   * @returns {Point}
   */
  getMouseTapEndPoint() {
    return this.mouseTapEndPoint;
  }
}
