import { Svg } from '@svgdotjs/svg.js';
import { LayersChangedEvent, LayersChangedEventArgs } from '../events/layers';
import { SelectionContext } from './selection';
import { SvgCanvasContext } from './svgcanvas';

const defaultLayerName = 'Layer';

export class LayersContext {
  /**
   *
   * @param {SvgCanvasContext} svgcanvasContext
   * @param {SelectionContext} selectionContext
   */
  constructor(svgcanvasContext, selectionContext) {
    /**
     * @private
     */
    this._svgcanvasContext = svgcanvasContext;
    /**
     * @private
     */
    this._selectionContext = selectionContext;
    /**
     * @type {Map<string,Svg>}
     * @private
     */
    this.layers = new Map();
    /**
     * @type {Svg} currentLayer
     * @private
     */
    this.currentLayer = null;
    /**
     * @private
     */
    this.currentLayerName = null;
    /**
     * @private
     */
    this.layersStupidMarker = 0;
    this.layersChanged = new LayersChangedEvent();
  }

  /**
   * @private
   */
  getLayersNames() {
    const layersNames = [];
    this.layers.forEach((value, key) => {
      layersNames.push(key);
    });
    return layersNames;
  }

  /**
   *
   * @returns {Svg}
   */
  getCurrentLayer() {
    return this.currentLayer;
  }

  getCurrentLayerName() {
    return this.currentLayerName;
  }

  getLayerByName(layerName) {
    if (this.layers.has(layerName)) {
      return this.layers.get(layerName);
    }
  }

  copyAllFromOneLayerToAnother(sourceLayerName, targetLayerName) {
    if (this.layers.has(sourceLayerName) && this.layers.has(targetLayerName)) {
      const sourceLayer = this.layers.get(sourceLayerName);
      const targetLayer = this.layers.get(targetLayerName);
      sourceLayer.children().forEach((el) => {
        targetLayer.add(el.clone());
      });
    }
  }

  /**
   * @private
   */
  notifyClientsAboutChanges() {
    this.layersChanged.fire(
      new LayersChangedEventArgs(this.getLayersNames(), this.currentLayerName)
    );
  }

  createLayer(name = `${defaultLayerName}${this.layersStupidMarker}`) {
    if (this.layers.has(name)) {
      this.layers.get(name).remove();
    }
    this.layers.set(name, this._svgcanvasContext.createLayer());
    ++this.layersStupidMarker;
    this.selectCurrentLayer(name);

    this.notifyClientsAboutChanges();
    return this.layers.get(name);
  }

  removeLayer(name) {
    if (this.layers.has(name) && this.layers.size > 1) {
      this.layers.get(name).remove();
      this.layers.delete(name);

      if (this.currentLayerName == name) {
        this.selectAnyAccessibleLayer();
      }

      this.notifyClientsAboutChanges();
    }
  }

  /**
   * @private
   */
  selectAnyAccessibleLayer() {
    const firstAccessibleLayerName = this.layers.keys().next().value;

    if (!!firstAccessibleLayerName) {
      this.selectCurrentLayer(firstAccessibleLayerName);
      return true;
    }
    return false;
  }

  selectCurrentLayer(name) {
    if (this.layers.has(name)) {
      this.currentLayer = this.layers.get(name);
      this.currentLayerName = name;
      this.disableAnotherLayers();

      this.notifyClientsAboutChanges();
    }
  }

  enableLayer(name) {
    if (this.layers.has(name)) {
      this.layers.get(name).node.style.removeProperty('pointer-events');
    }
  }

  disableLayer(name) {
    if (this.layers.has(name)) {
      this.layers.get(name).node.style.pointerEvents = 'none';
    }
  }

  disableAnotherLayers() {
    this.layers.forEach((value, key) => {
      if (key !== this.currentLayerName) {
        value.node.style.pointerEvents = 'none';
      }
    });
  }

  disableCurrentLayer() {
    this.disableLayer(this.getCurrentLayerName());
  }
  enableCurrentLayer() {
    this.enableLayer(this.getCurrentLayerName());
  }

  addElementToLayer(el) {
    this.currentLayer.add(el);
  }
}
