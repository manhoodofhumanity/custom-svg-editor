import { TOOLS } from '../../constants/tools';

/**
 * @param {import('../initContexts').Contexts} contexts
 */
export function configureLayersContextSubscriptions({
  layersContext,
  toolsContext,
}) {
  // subscribe on tool change event, to disable dragging elements, if we tries to draw(on them)
  toolsContext.toolChangedEvent.addHandler(({ tool }) => {
    if (tool !== TOOLS.SELECTION) {
      layersContext.disableCurrentLayer();
    } else {
      layersContext.enableCurrentLayer();
    }
  });
}
