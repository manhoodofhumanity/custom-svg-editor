/**
 * @param {import('./initContexts').Contexts} contexts
 */
export function layersContextPreconfigurations({ layersContext }) {
  layersContext.createLayer();
}
