import { G } from '@svgdotjs/svg.js';
import { TOOLS } from '../constants/tools';

/**
 *
 * @param {import('./initContexts').Contexts} contexts
 */
export function uiContextPreconfigurations({
  toolsContext,
  uiContext,
  layersContext,
  selectionContext,
}) {
  // mount tools selection buttons
  uiContext.mountClickEventToElementById('selection-tool-button', () => {
    toolsContext.selectCurrentTool(TOOLS.SELECTION);
  });

  uiContext.mountClickEventToElementById('line-tool-button', () => {
    toolsContext.selectCurrentTool(TOOLS.LINE);
  });

  uiContext.mountClickEventToElementById('ellipse-tool-button', () => {
    toolsContext.selectCurrentTool(TOOLS.ELLIPSE);
  });

  uiContext.mountClickEventToElementById('rect-tool-button', () => {
    toolsContext.selectCurrentTool(TOOLS.RECTANGLE);
  });

  uiContext.mountClickEventToElementById('image-tool-button', () => {
    toolsContext.selectCurrentTool(TOOLS.IMAGE);
  });

  uiContext.mountClickEventToElementById('zoom-tool-button', () => {
    toolsContext.selectCurrentTool(TOOLS.ZOOM);
  });

  // mount layers toolbar buttons
  uiContext.mountClickEventToElementById('create-layer-button', () => {
    const inputtedName = prompt('Layer name: ') || undefined;
    layersContext.createLayer(inputtedName);
  });

  // delete current layer ( we dont need render delete buttons for every list item)
  uiContext.mountClickEventToElementById('remove-layer-button', () => {
    layersContext.removeLayer(layersContext.getCurrentLayerName());
  });

  uiContext.mountOnInputEventToElementById(
    'selected-elem-classname-input',
    (e) => {
      selectionContext.changeSelectedElementClass(e.target.value);
    }
  );

  uiContext.mountClickEventToElementById('group-elements-tool', () => {
    layersContext.addElementToLayer(selectionContext.groupSelectedElements());
  });

  uiContext.mountClickEventToElementById('ungroup-elements-tool', () => {
    selectionContext.ungroupSelectedGroup();
  });

  uiContext.mountClickEventToElementById('lsystem-tool-button', () => {
    const axiomLayer = layersContext.getLayerByName('axiom');
    const rulesLayer = layersContext.getLayerByName('rules');
    const tempLayer = layersContext.createLayer('temp');
    const resultLayer = layersContext.createLayer('L-result');

    const iterationsCount = Number.parseInt(
      prompt('Count of iterations: ', '1')
    );

    // copy all from axiom layer to result layer
    layersContext.copyAllFromOneLayerToAnother('axiom', 'L-result');

    for (let i = 1; i <= iterationsCount; ++i) {
      // copy all from result layer to temp layer
      layersContext.copyAllFromOneLayerToAnother('L-result', 'temp');

      // clear content of result layer
      resultLayer.clear();
      // using temp layer generate result of next L-system iteration to Result layer
      tempLayer.children().forEach((replacingElem) => {
        if (!!replacingElem.node.className.baseVal) {
          // search for the appropriate rule
          const appropriateRuleElem = rulesLayer
            .findOne(`.for${replacingElem.node.className.baseVal}`)
            .clone();

          const replacingElemBox =
            replacingElem instanceof G
              ? replacingElem.bbox()
              : replacingElem.rbox();

          const ruleElemBox =
            appropriateRuleElem instanceof G
              ? appropriateRuleElem.bbox()
              : appropriateRuleElem.rbox();

          // translate to position of replacing elem
          // todo: resolve move issue (result is moving out from exiom, but need to be inside of axiom)
          appropriateRuleElem.move(replacingElemBox.x, replacingElemBox.y);

          // scale rule to the size of current replacing element?

          const ratioW = ruleElemBox.width / replacingElemBox.width;
          const ratioH = ruleElemBox.height / replacingElemBox.height;

          appropriateRuleElem.transform({
            scaleX: 1 / ratioW,
            scaleY: 1 / ratioH,
            origin: 'top left',
          });

          // add resized and positioned new elem to result  layer
          resultLayer.add(appropriateRuleElem);

          // ungroup rule that was presented as group
          if (appropriateRuleElem instanceof G) {
            appropriateRuleElem.ungroup();
          }
        }
      });
      // clear content of temp layer
      tempLayer.clear();
    }
    layersContext.removeLayer('temp');
    resultLayer.move(0, 0);
  });
}
